//
//  LoadingStateView.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LoadingStateView: UIView {
    
    struct ErrorHandler {
        let error: String
        let handler: (() -> ())?
    }
    
    enum State {
        case idle
        case loading
        case error(ErrorHandler)
    }
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var retryButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Retry", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var activity: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .gray)
        view.color = self.tintColor
        view.hidesWhenStopped = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var dispose = DisposeBag()
    
    // MARK: - lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        addSubview(retryButton)
        addSubview(activity)
        
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 30).isActive = true
        label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30).isActive = true
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -20).isActive = true
        
        retryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 8).isActive = true
        retryButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        activity.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activity.centerYAnchor.constraint(equalTo: retryButton.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - functions
    
    func setState(_ state: State) {
        switch state {
        case .idle:
            activity.stopAnimating()
            label.text = nil
            retryButton.isHidden = true
        case .loading:
            label.text = "Loading..."
            label.textColor = .darkText
            activity.startAnimating()
            retryButton.isHidden = true
        case .error(let error):
            label.text = error.error
            label.textColor = .red
            activity.stopAnimating()
            
            if let handler = error.handler {
                dispose = DisposeBag()
                retryButton.isHidden = false
                retryButton.rx.tap
                    .subscribe(onNext: { _ in
                        handler()
                    })
                    .disposed(by: dispose)
            } else {
                retryButton.isHidden = true
            }
        }
    }
    
}

extension Reactive where Base: LoadingStateView {
    
    var state: Binder<LoadingStateView.State> {
        return Binder(self.base) { view, state in
            view.setState(state)
        }
    }
}
