//
//  Int+Formating.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import Foundation

private let numberFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.groupingSeparator = " "
    return formatter
}()

extension Int32 {
    
    func toPeopleString() -> String {
        let string = numberFormatter.string(from: NSNumber(value: self)) ?? String(self)
        switch self {
        case 1:
            return string + " person"
        default:
            return string + " people"
        }
    }
    
}
