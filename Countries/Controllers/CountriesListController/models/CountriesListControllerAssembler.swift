//
//  CountriesListControllerAssembler.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit

/// Assembler для создания экрана и зависимостей для «List of countries»
protocol CountriesListControllerAssembler {
    
    /// Инициализация контроллера «List of countries»
    ///
    /// - Parameters:
    ///   - router: Роутер
    ///   - parameters: Параметры
    ///   - delegate: Далегат
    func createCountriesListController(router: CountriesListRouter,
                                       parameters: CountriesListViewModel.Parameters,
                                       delegate: CountriesListDelegate?) -> UIViewController
    
}

extension DependenciesStorage: CountriesListControllerAssembler {
    
    /// Инициализация контроллера «List of countries»
    ///
    /// - Parameters:
    ///   - router: Роутер
    ///   - parameters: Параметры
    ///   - delegate: Далегат
    func createCountriesListController(router: CountriesListRouter,
                                       parameters: CountriesListViewModel.Parameters,
                                       delegate: CountriesListDelegate?) -> UIViewController {
        let viewModel = CountriesListViewModel(assembler: self,
                                               router: router,
                                               facade: networkFacade,
                                               parameters: parameters)
        viewModel.delegate = delegate
        return CountriesListController(with: viewModel)
    }
    
}
