//
//  CountriesListRouter.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit

/// Роутер для экрана «List of countries»
protocol CountriesListRouter: CountryDetailRouter {
    
    /// Открытие контроллера «List of countries»
    ///
    /// - Parameters:
    ///   - parameters: Параметры
    ///   - delegate: Делегат
    func routeToCountriesListController(parameters: CountriesListViewModel.Parameters,
                                        delegate: CountriesListDelegate?)
    
}

extension Router: CountriesListRouter {
    
    /// Открытие контроллера «List of countries»
    ///
    /// - Parameters:
    ///   - parameters: Параметры
    ///   - delegate: Делегат
    func routeToCountriesListController(parameters: CountriesListViewModel.Parameters,
                                        delegate: CountriesListDelegate?) {
        let viewController = assembler.createCountriesListController(router: self,
                                                                     parameters: parameters,
                                                                     delegate: delegate)
        routeTo(viewController, animated: true)
    }
    
}
