//
//  CountriesListViewModel.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

/// Протокол для общения с контроллером
protocol CountriesListViewModelDelegate: class {
    
}

/// Протокол для общения с внешним миром
protocol CountriesListDelegate: class {
    
}

/// Вьюмодель для экрана «List of countries»
final class CountriesListViewModel {
    
    /// Параметры
    struct Parameters { }

    /// Сборщик секций
    private let assembler: CountriesListControllerAssembler
    /// Роутер для работы экрана
    private let router: CountriesListRouter
    
    /// Facade for data
    private let facade: NetworkFacade
    
    private lazy var stateSubject = BehaviorRelay<LoadingStateView.State>(value: .idle)
    
    private lazy var countriesSubject = BehaviorRelay<[CountrySmall]>(value: [])
    
    private let disposeBag = DisposeBag()
    
    // MARK: - lifecycle

    init(assembler: CountriesListControllerAssembler,
         router: CountriesListRouter,
         facade: NetworkFacade,
         parameters: Parameters) {
        self.assembler = assembler
        self.router = router
        self.facade = facade
    }
    
    // MARK: - proeprties
    
    /// Делегат для общения с контроллером
    weak var vmDelegate: CountriesListViewModelDelegate?
    
    /// Делегат для общения с внешним миром
    weak var delegate: CountriesListDelegate?

    // MARK: - getters

    /// Название для экрана
    var screenName: String? {
        return "List of countries"
    }
    
    var state: Driver<LoadingStateView.State> {
        return stateSubject.asDriver()
    }
    
    var countries: Driver<[CountrySmall]> {
        return countriesSubject.asDriver()
    }
    
    let searchText = PublishRelay<String>()
    
    let didSelectCountry = PublishRelay<IndexPath>()
    
    let refresh = PublishRelay<Void>()

    // MARK: - actions
    
    /// Запуск работы вьюмодели
    func setup() {
        fetchCountries()
        setupObserving()
    }

    // MARK: - private
    
    private func setupObserving() {
        searchText
            .distinctUntilChanged()
            .skip(1)
            .do(onNext: { [weak self] _ in
                if case .loading? = self?.stateSubject.value {
                    return
                }
                self?.countriesSubject.accept([])
                self?.stateSubject.accept(.loading)
            })
            .debounce(0.5, scheduler: MainScheduler.asyncInstance)
            .subscribe(onNext: { [weak self] text in
                self?.fetchCountries(searchString: text.isEmpty ? nil : text)
            })
            .disposed(by: disposeBag)
        
        refresh
            .subscribe(onNext: { [weak self] in
                self?.fetchCountries()
            })
            .disposed(by: disposeBag)
        
        didSelectCountry
            .subscribe(onNext: { [weak self] ip in
                guard let self = self else { return }
                let country = self.countriesSubject.value[ip.row]
                self.routeToCountryDetail(country)
            })
            .disposed(by: disposeBag)
    }
    
    private func fetchCountries(searchString: String? = nil) {
        let request: Single<[CountrySmall]>
        if let string = searchString {
            request = facade.searchCountry(byName: string)
        } else {
            request = facade.allContries()
        }
        
        request
            .do(onSubscribed: { [weak self] in
                self?.stateSubject.accept(.loading)
            })
            .subscribe { [weak self] event in
                guard let self = self else { return }
                
                switch event {
                case .success(let countries):
                    self.countriesSubject.accept(countries)
                    self.stateSubject.accept(.idle)
                case .error(let error):
                    let handler: (() -> ())? = (error is ResponseError) ? nil : { [weak self] in
                        self?.fetchCountries(searchString: searchString)
                    }
                    let errorHandler = LoadingStateView.ErrorHandler(error: error.localizedDescription, handler: handler)
                    self.stateSubject.accept(.error(errorHandler))
                }
            }
            .disposed(by: disposeBag)
    }
    
    private func routeToCountryDetail(_ country: CountrySmall) {
        let params = CountryDetailViewModel.Parameters(countryId: country.alpha3Code,
                                                       countryName: country.name)
        router.routeToCountryDetailController(parameters: params,
                                              delegate: nil)
    }
    
}
