//
//  CountriesListController.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/// Константы контроллера
private enum Constants {
    
    /// Идентификатор переиспользования для ячеек
    static let cellReuseIdentifier = "cellReuseIdentifier"
    
}

/// Контроллер для экрана «List of countries»
final class CountriesListController: UITableViewController {

    private lazy var loadingStateView = LoadingStateView(frame: view.bounds)
    
    /// Вью модель
    private let viewModel: CountriesListViewModel
    
    private let disposeBag = DisposeBag()
    
    private var errorHandlerDisposeBag = DisposeBag()
    
    // MARK: - View Controller Lifecycle

    init(with viewModel: CountriesListViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)

        viewModel.vmDelegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        viewModel.setup()
    }

    // MARK: - Private

    /// Метод настройки экрана
    private func setup() {
        view.backgroundColor = UIColor.white
        title = viewModel.screenName
        definesPresentationContext = true
        
        tableView.refreshControl = UIRefreshControl()
        tableView.register(CountryCell.self, forCellReuseIdentifier: Constants.cellReuseIdentifier)
        tableView.backgroundView = loadingStateView
        tableView.dataSource = nil
        tableView.delegate = nil
        tableView.tableFooterView = UIView()
        
        setupSearchBar()
        setupBindings()
    }
    
    private func setupBindings() {
        viewModel.countries.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: Constants.cellReuseIdentifier, cellType: CountryCell.self)) { (row, element, cell) in
                cell.configure(with: element)
            }
            .disposed(by: disposeBag)
        
        viewModel.state
            .do(onNext: { [weak self] state in
                switch state {
                case .idle, .error:
                    self?.tableView.refreshControl?.endRefreshing()
                default:
                    break
                }
            })
            .asObservable()
            .bind(to: loadingStateView.rx.state)
            .disposed(by: disposeBag)
        
        tableView?.refreshControl?.rx.controlEvent(.valueChanged)
            .bind(to: viewModel.refresh)
            .disposed(by: disposeBag)
        
        tableView?.rx.itemSelected
            .do(onNext: { [weak self] ip in
                self?.tableView.deselectRow(at: ip, animated: true)
            })
            .bind(to: viewModel.didSelectCountry)
            .disposed(by: disposeBag)
    }
    
    private func setupSearchBar() {
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        self.navigationItem.searchController = search
    }
    
}

// MARK: - RootViewModelDelegate
extension CountriesListController: CountriesListViewModelDelegate {

}

// MARK: - UISearchResultsUpdating
extension CountriesListController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.searchText.accept(searchController.searchBar.text ?? "")
    }
    
}
