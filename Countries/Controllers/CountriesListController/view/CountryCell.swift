//
//  CountryCell.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: nil)
        
        accessoryType = .disclosureIndicator
        detailTextLabel?.textColor = UIColor.darkText
        backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - functions
    
    func configure(with country: CountrySmall) {
        textLabel?.text = country.name
        detailTextLabel?.text = "Population: \(country.population.toPeopleString())"
    }
    
}
