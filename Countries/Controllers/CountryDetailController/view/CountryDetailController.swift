//
//  CountryDetailController.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/// Константы контроллера
private enum Constants {
    
    static let textCellId = "textCellId"
    
    static let countryCellId = "countryCellId"
    
}

/// Контроллер для экрана «Detail of Country»
final class CountryDetailController: UIViewController {
    
    private lazy var loadingStateView = LoadingStateView(frame: view.bounds)
    
    private let tableView: UITableView = UITableView(frame: UIScreen.main.bounds, style: .grouped)

    /// Вью модель
    private let viewModel: CountryDetailViewModel
    
    private let disposeBag = DisposeBag()
    
    private var sections: [CountryDetailViewModel.Section] = []
    
    // MARK: - View Controller Lifecycle

    init(with viewModel: CountryDetailViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)

        viewModel.vmDelegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = tableView
        tableView.dataSource = self
        tableView.register(CountryCell.self, forCellReuseIdentifier: Constants.countryCellId)
        tableView.backgroundView = loadingStateView
        tableView.tableFooterView = UIView()
        tableView.refreshControl = UIRefreshControl()
        loadingStateView.backgroundColor = .groupTableViewBackground
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        viewModel.setup()
    }

    // MARK: - Private

    /// Метод настройки экрана
    private func setup() {
        view.backgroundColor = UIColor.white
        title = viewModel.screenName
        navigationItem.largeTitleDisplayMode = .never
        
        setupBindings()
    }

    private func setupBindings() {
        viewModel.sections
            .drive(onNext: { [weak self] sections in
                self?.sections = sections
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
        
        viewModel.state
            .do(onNext: { [weak self] state in
                switch state {
                case .idle, .error:
                    self?.tableView.refreshControl?.endRefreshing()
                default:
                    break
                }
            })
            .asObservable()
            .bind(to: loadingStateView.rx.state)
            .disposed(by: disposeBag)
        
        tableView.refreshControl?.rx.controlEvent(.valueChanged)
            .bind(to: viewModel.refresh)
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .do(onNext: { [weak self] ip in
                self?.tableView.deselectRow(at: ip, animated: true)
            })
            .bind(to: viewModel.didSelectField)
            .disposed(by: disposeBag)
    }
}

extension CountryDetailController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections[indexPath.section].items[indexPath.row]
        
        let cell = self.cell(for: item, tableView: tableView, indexPath: indexPath)
        switch item {
        case .country(let country):
            if let countryCell = cell as? CountryCell {
                countryCell.configure(with: country)
            }
        case .text(let text):
            cell.textLabel?.text = text
            cell.detailTextLabel?.text = nil
        case .titledValue(let title, let value):
            cell.textLabel?.text = title
            cell.detailTextLabel?.text = value
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
    private func cell(for item: CountryDetailViewModel.Item, tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch item {
        case .country:
            return tableView.dequeueReusableCell(withIdentifier: Constants.countryCellId, for: indexPath)
        case .text, .titledValue:
            if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.textCellId) {
                return cell
            }
            let cell = UITableViewCell(style: .value1, reuseIdentifier: Constants.textCellId)
            cell.backgroundColor = UIColor.white
            return cell
        }
    }
    
}

// MARK: - RootViewModelDelegate
extension CountryDetailController: CountryDetailViewModelDelegate {

}
