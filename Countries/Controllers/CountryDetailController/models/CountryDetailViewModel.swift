//
//  CountryDetailViewModel.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

/// Протокол для общения с контроллером
protocol CountryDetailViewModelDelegate: class {
    
}

/// Протокол для общения с внешним миром
protocol CountryDetailDelegate: class {
    
}

/// Вьюмодель для экрана «Detail of Country»
final class CountryDetailViewModel {
    
    /// Параметры
    struct Parameters {
        let countryId: CountryId
        let countryName: String
    }
    
    enum Item {
        case titledValue(title: String, value: String)
        case text(String)
        case country(CountrySmall)
    }
    
    struct Section {
        let title: String
        let items: [Item]
    }
    
    private let countryId: CountryId
    
    private let countryName: String

    /// Сборщик секций
    private let assembler: CountryDetailControllerAssembler
    /// Роутер для работы экрана
    private let router: CountryDetailRouter
    
    /// Network facade
    private let facade: NetworkFacade
    
    private lazy var stateSubject = BehaviorRelay<LoadingStateView.State>(value: .idle)
    
    private lazy var sectionsSubject = BehaviorRelay<[Section]>(value: [])
    
    private let disposeBag = DisposeBag()

    init(assembler: CountryDetailControllerAssembler,
         router: CountryDetailRouter,
         facade: NetworkFacade,
         parameters: Parameters) {
        self.assembler = assembler
        self.router = router
        self.facade = facade
        
        countryId = parameters.countryId
        countryName = parameters.countryName
    }
    
    // MARK: - properties
    
    /// Делегат для общения с контроллером
    weak var vmDelegate: CountryDetailViewModelDelegate?
    
    /// Делегат для общения с внешним миром
    weak var delegate: CountryDetailDelegate?

    // MARK: - getters

    /// Название для экрана
    var screenName: String? {
        return countryName
    }
    
    var state: Driver<LoadingStateView.State> {
        return stateSubject.asDriver()
    }
    
    var sections: Driver<[Section]> {
        return sectionsSubject.asDriver()
    }
    
    let didSelectField = PublishRelay<IndexPath>()
    
    let refresh = PublishRelay<Void>()

    // MARK: - actions
    
    /// Запуск работы вьюмодели
    func setup() {
        fetchCountry()
        setupObserving()
    }

    // MARK: - private
    
    private func fetchCountry() {
        facade.country(byCode: countryId)
            .do(onSubscribed: { [weak self] in
                self?.stateSubject.accept(.loading)
            })
            .flatMap { country in
                return self.facade.countries(byCodes: country.borders)
                    .map { borderCountries in
                        return self.parseSections(country: country, borderCountries: borderCountries)
                }
            }
            .subscribe { [weak self] event in
                guard let self = self else { return }
                
                switch event {
                case .success(let sections):
                    self.sectionsSubject.accept(sections)
                    self.stateSubject.accept(.idle)
                case .error(let error):
                    let handler: (() -> ())? = (error is ResponseError) ? nil : { [weak self] in
                        self?.fetchCountry()
                    }
                    let errorHandler = LoadingStateView.ErrorHandler(error: error.localizedDescription, handler: handler)
                    self.sectionsSubject.accept([])
                    self.stateSubject.accept(.error(errorHandler))
                }
            }
            .disposed(by: disposeBag)
    }
    
    private func setupObserving() {
        refresh
            .subscribe(onNext: { [weak self] in
                self?.fetchCountry()
            })
            .disposed(by: disposeBag)
        didSelectField
            .subscribe(onNext: { [weak self] ip in
                guard let self = self else { return }
                
                let item = self.sectionsSubject.value[ip.section].items[ip.row]
                switch item {
                case .country(let country):
                    self.routeToCountryDetail(country)
                default:
                    break
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func parseSections(country: CountryDetailed, borderCountries: [CountrySmall]) -> [Section] {
        let name = Item.titledValue(title: "Name", value: country.name)
        let capital = Item.titledValue(title: "Capital", value: country.capital)
        let population = Item.titledValue(title: "Population", value: country.population.toPeopleString())
        let borderCountriesItems = borderCountries.map { Item.country($0) }
        let currencies: [Item] = country.currencies.map { cur in
            var text = cur.name
            if let symbol = cur.symbol, !symbol.isEmpty {
                text = "\(symbol) - \(text)"
            }
            return Item.text(text)
        }
        
        let emptyItem = Item.text("Nothing to show")
        
        return [
            Section(title: "Information",
                    items: [name, capital, population]),
            Section(title: "Borders",
                    items: borderCountriesItems.isEmpty ? [emptyItem] : borderCountriesItems),
            Section(title: "Currencies",
                    items: currencies.isEmpty ? [emptyItem] : currencies),
        ]
    }
    
    private func routeToCountryDetail(_ country: CountrySmall) {
        let params = CountryDetailViewModel.Parameters(countryId: country.alpha3Code,
                                                       countryName: country.name)
        router.routeToCountryDetailController(parameters: params,
                                              delegate: nil)
    }
    
}

// MARK: - Datasource
extension CountryDetailViewModel {

}
