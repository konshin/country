//
//  CountryDetailControllerAssembler.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit

/// Assembler для создания экрана и зависимостей для «Detail of Country»
protocol CountryDetailControllerAssembler {
    
    /// Инициализация контроллера «Detail of Country»
    ///
    /// - Parameters:
    ///   - router: Роутер
    ///   - parameters: Параметры
    ///   - delegate: Далегат
    func createCountryDetailController(router: CountryDetailRouter,
                                       parameters: CountryDetailViewModel.Parameters,
                                       delegate: CountryDetailDelegate?) -> UIViewController
    
}

extension DependenciesStorage: CountryDetailControllerAssembler {
    
    /// Инициализация контроллера «Detail of Country»
    ///
    /// - Parameters:
    ///   - router: Роутер
    ///   - parameters: Параметры
    ///   - delegate: Далегат
    func createCountryDetailController(router: CountryDetailRouter,
                                       parameters: CountryDetailViewModel.Parameters,
                                       delegate: CountryDetailDelegate?) -> UIViewController {
        let viewModel = CountryDetailViewModel(assembler: self,
                                               router: router,
                                               facade: networkFacade,
                                               parameters: parameters)
        viewModel.delegate = delegate
        return CountryDetailController(with: viewModel)
    }
    
}
