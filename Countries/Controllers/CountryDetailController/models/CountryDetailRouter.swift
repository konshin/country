//
//  CountryDetailRouter.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit

/// Роутер для экрана «Detail of Country»
protocol CountryDetailRouter {
    
    /// Открытие контроллера «Detail of Country»
    ///
    /// - Parameters:
    ///   - parameters: Параметры
    ///   - delegate: Делегат
    func routeToCountryDetailController(parameters: CountryDetailViewModel.Parameters,
                                              delegate: CountryDetailDelegate?)
    
}

extension Router: CountryDetailRouter {
    
    /// Открытие контроллера «Detail of Country»
    ///
    /// - Parameters:
    ///   - parameters: Параметры
    ///   - delegate: Делегат
    func routeToCountryDetailController(parameters: CountryDetailViewModel.Parameters,
                                              delegate: CountryDetailDelegate?) {
        let viewController = assembler.createCountryDetailController(router: self,
                                                                     parameters: parameters,
                                                                     delegate: delegate)
        routeTo(viewController, animated: true)
    }
    
}
