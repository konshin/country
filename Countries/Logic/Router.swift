//
//  Router.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import UIKit

final class Router {
    
    private weak var rootController: UINavigationController?
    
    let assembler: DependenciesStorage
    
    init(rootController: UINavigationController?, assembler: DependenciesStorage) {
        self.rootController = rootController
        self.assembler = assembler
    }
    
    func routeTo(_ viewController: UIViewController, animated: Bool) {
        rootController?.pushViewController(viewController, animated: animated)
    }
    
}
