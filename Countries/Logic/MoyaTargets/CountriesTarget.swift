//
//  CountriesTarget.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import Moya

enum CountriesTarget {
    case all
    case name(String)
    case code(CountryId)
    case codes([CountryId])
}

extension CountriesTarget: TargetType {
    
    var baseURL: URL {
        return NSURL(string: "https://restcountries.eu")! as URL
    }
    
    var path: String {
        switch self {
        case .all:
            return "/rest/v2/all"
        case .name(let name):
            return "rest/v2/name/\(name)"
        case .code(let code):
            return "/rest/v2/alpha/\(code)"
        case .codes(let codes):
            let params = codes.joined(separator: ";")
            return "/rest/v2/alpha?codes=\(params)"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
    
}
