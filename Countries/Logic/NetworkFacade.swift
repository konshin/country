//
//  NetworkFacade.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import Moya
import RxSwift

final class NetworkFacade {
    
    enum Error: Swift.Error, LocalizedError {
        case decoding(Swift.Error)
        case status(Int)
        
        var errorDescription: String? {
            switch self {
            case .decoding:
                return "Can't parse the network data"
            case .status:
                return "Unknown error"
            }
        }
    }
    
    private let provider: MoyaProvider<CountriesTarget>
    
    private let jsonDecoder = JSONDecoder()
    
    init() {
        provider = MoyaProvider()
    }
    
    // MARK: - functions
    
    private func performRequest<T: Decodable>(target: CountriesTarget) -> Single<T> {
        return provider.rx.request(target)
            .flatMap { response in
                guard 200...299 ~= response.statusCode else {
                    let responseError = try? self.jsonDecoder.decode(ResponseError.self, from: response.data)
                    return Single.error(responseError ?? Error.status(response.statusCode))
                }
                
                do {
                    let result = try self.jsonDecoder.decode(T.self, from: response.data)
                    return Single.just(result)
                } catch {
                    return Single.error(Error.decoding(error))
                }
            }
            .observeOn(MainScheduler.asyncInstance)
    }
    
}

extension NetworkFacade {
    
    func allContries() -> Single<[CountrySmall]> {
        return performRequest(target: .all)
    }
    
    func searchCountry(byName partOfName: String) -> Single<[CountrySmall]> {
        return performRequest(target: .name(partOfName))
    }
    
    func country(byCode code: CountryId) -> Single<CountryDetailed> {
        return performRequest(target: .code(code))
    }
    
    func countries(byCodes codes: [CountryId]) -> Single<[CountrySmall]> {
        guard !codes.isEmpty else {
            return Single.just([])
        }
        
        // We cant use API method https://restcountries.eu/rest/v2/alpha?codes={code};{code};{code}
        // because of force encoding semicolon by URL :(
        let requests: [Observable<CountrySmall>] = codes.map { performRequest(target: .code($0)).asObservable() }
        return Observable.combineLatest(requests)
            .asSingle()
    }
    
}
