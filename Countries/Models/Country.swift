//
//  Country.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import Foundation

typealias CountryId = String

struct CountrySmall: Decodable {
    let alpha3Code: CountryId
    let name: String
    let population: Int32
}

struct CountryDetailed: Decodable {
    let alpha3Code: CountryId
    let name: String
    let population: Int32
    let capital: String
    let borders: [CountryId]
    let currencies: [Currency]
}
