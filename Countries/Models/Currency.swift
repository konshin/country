//
//  Currency.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import Foundation

struct Currency: Decodable {
    let code: String
    let name: String
    let symbol: String?
}
