//
//  ResponseError.swift
//  Countries
//
//  Created by akonshin on 10/07/2019.
//  Copyright © 2019 Konshin. All rights reserved.
//

import Foundation

struct ResponseError: Error, LocalizedError, Decodable {
    let status: Int
    let message: String
    
    var errorDescription: String? {
        return message
    }
}
